# Build the app
FROM rust:1.75 as builder
WORKDIR /usr/src/actix_web_app
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

# This dummy build helps to cache the dependencies which will not change often
RUN mkdir src && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/actix_web_app*

# Now copy in the actual source code and do the final build
COPY ./src ./src
RUN cargo build --release

# Build the final image
FROM debian:bookworm-slim
COPY --from=builder /usr/src/actix_web_app/target/release/actix_web_app /usr/local/bin

# Run the app
CMD ["actix_web_app"]
