use actix_web::{web, App, HttpResponse, HttpServer, Responder, Result};
use serde::{Deserialize, Serialize};

// Define a simple structure to deserialize the JSON data into.
#[derive(Serialize, Deserialize)]
struct Echo {
    message: String,
}

// The handler function for the POST request at "/echo"
// It accepts a JSON object, deserializes it, and then echoes it back.
async fn echo(echo: web::Json<Echo>) -> Result<impl Responder> {
    Ok(HttpResponse::Ok().json(Echo {
        message: echo.message.clone(),
    }))
}


async fn greet() -> impl Responder {
    HttpResponse::Ok().content_type("text/plain").body("Hello, Actix!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/", web::get().to(greet)) // Route for the GET request
            .route("/echo", web::post().to(echo)) // Route for the POST request
    })
    .bind("0.0.0.0:8080")?
    .run()
    .await
}
