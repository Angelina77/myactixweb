# Rust Actix Web App

This project is a simple Rust Actix web application that responds to GET requests at the root (`/`) with the message "Hello, Actix!".

## Requirements

- Rust Programming Language
- Cargo (Rust's package manager and build tool)
- Docker


## Local Development

### Building the Application

To build the application locally, run the following commands:

```sh
# Clone the repository
git clone <your-repository-url>
cd <your-repository-directory>
```

- Build the project with Cargo
```sh
cargo build --release
```

- Running the Application
After a successful build, you can run the application using:

```sh
cargo run --release
```
The server will start listening on http://localhost:8080.

## Building and Running with Docker

The application can be containerized using the provided Dockerfile, which packages the application into a Docker image and sets it up to run inside a container.

Building the Docker Image
In the project directory, build the Docker image using:

```sh
docker build -t actix_web_app .
```
This command creates a Docker image named actix_web_app.

Running the Docker Container
To run the application inside a Docker container, execute:

```sh
docker run -d -p 8080:8080 actix_web_app
```
This starts the container in detached mode and maps port 8080 from the container to port 8080 on the host.


You can then access the application at http://localhost:8080.

## screenshots
docker running
![alt text](image.png)

web browser

![alt text](image-1.png)




